// Canned functionality for JIRA Activity
 $(function() {
     "use strict";

     // Get parameters from query string
     // and stick them in an object
     function getQueryParams(qs) {
         qs = qs.split("+").join(" ");

         var params = {}, tokens,
             re = /[?&]?([^=]+)=([^&]*)/g;

         while (tokens = re.exec(qs)) {
             params[decodeURIComponent(tokens[1])] =
                 decodeURIComponent(tokens[2]);
         }

         return params;
     }

     var params = getQueryParams(document.location.search);
     var baseUrl = params.xdm_e + params.cp;
  
     delete params.jwt;
     var redirectParams = $.param(params), uri;
 
     console.log(JSON.stringify(params));

     // uri = '/activity?projectId={project.id}' + redirectParams +
     //    '&jwt=' + $('meta[name=token]').attr('content');

     // window.location.replace(uri);

 });