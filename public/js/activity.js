// Canned functionality for JIRA Activity
 $(function() {
     "use strict";

     // Get parameters from query string
     // and stick them in an object
     function getQueryParams(qs) {
         qs = qs.split("+").join(" ");

         var params = {}, tokens,
             re = /[?&]?([^=]+)=([^&]*)/g;

         while (tokens = re.exec(qs)) {
             params[decodeURIComponent(tokens[1])] =
                 decodeURIComponent(tokens[2]);
         }

         return params;
     }

     AP.define('JiraActivity', {
         buildProjectTable: function(projects, selector) {
            var params = getQueryParams(document.location.search);
            var baseUrl = params.xdm_e + params.cp;

            console.log(JSON.stringify(params));
         }

     });
 });